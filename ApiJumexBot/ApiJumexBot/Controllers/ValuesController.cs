﻿using ApiJumexBot.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace ApiJumexBot.Controllers
{
    public class ValuesController : ApiController
    {

        private static string Con = ConfigurationManager.ConnectionStrings["jumexbot"].ConnectionString;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static SqlConnection Conexion() {

            return new SqlConnection(Con);
        }

        [HttpGet]
        [ActionName("getConsultaVacantes")]
        public IHttpActionResult getConsultaVacantes() {

            try
            {
                IEnumerable<string> headerValues;
                if (Request.Headers.TryGetValues("token", out headerValues))
                {
                    if (ValidacionToken(headerValues.FirstOrDefault()) > 0)
                    {
                        using (SqlConnection conn= Conexion())
                        {
                            conn.Open();
                            List<Vacantes> vacantes = new List<Vacantes>();
                            SqlCommand cmd = new SqlCommand("SELECT IdVacante,Puesto,Ubicacion FROM JumexBot_Vacantes",conn);
                            SqlDataReader rd = cmd.ExecuteReader();
                            if (rd.HasRows)
                            {
                                while (rd.Read())
                                {
                                    vacantes.Add(new Vacantes {
                                        IdVacante=rd.GetString(0),
                                        Puesto=rd.GetString(1),
                                        Ubicacion=rd.GetString(2)
                                    });
                                }
                                conn.Close();
                                return Json(vacantes.ToList());
                            }
                            else
                            {
                                return Json("No se han encontrado vacantes en la BD");
                            }
                        }
                    }
                    else
                    {
                        return Json(new {msg="Token invalido." });
                    }
                }
                else
                {
                    return Json(new {msg="Error al validar el token." });
                }

            }
            catch (Exception e)
            {

                return Json(new {msg=e.Message.ToString()});
            }

        }

        [HttpPost]
        [ActionName("postRequisitosVacante")]
        public IHttpActionResult postRequisitosVacante(Vacantes vacantes) {

            try
            {
                IEnumerable<string> headerValues;
                if (Request.Headers.TryGetValues("token", out headerValues))
                {
                    if (ValidacionToken(headerValues.FirstOrDefault()) > 0)
                    {
                        using (SqlConnection conn= Conexion())
                        {
                            List<Vacantes> vacantesrequisitos = new List<Vacantes>();
                            conn.Open();
                            string[] ubicacion = vacantes.Ubicacion.Split(' ');
                            SqlCommand cmd = new SqlCommand("SELECT Puesto,EdadMinima,Escolaridad,Experiencia,Ubicacion,PuestoJefeDirecto,Email FROM JumexBot_Vacantes WHERE Puesto LIKE @PUESTO AND Cercanias like @UBICACION AND Escolaridad like @ESCOLARIDAD AND Estatus=0",conn);
                            cmd.Parameters.AddWithValue("@PUESTO","%"+vacantes.Puesto+"%");
                            cmd.Parameters.AddWithValue("@UBICACION","%"+ubicacion[0]+"%");
                            cmd.Parameters.AddWithValue("@ESCOLARIDAD","%"+vacantes.Escolaridad+"%");
                            SqlDataReader rd = cmd.ExecuteReader();
                            if (rd.HasRows)
                            {
                                while (rd.Read())
                                {
                                    vacantesrequisitos.Add(new Vacantes
                                    {
                                        Puesto = rd.GetString(0),
                                        EdadMinima = rd.GetString(1),
                                        Escolaridad=rd.GetString(2),
                                        Experiencia=rd.GetString(3),
                                        Ubicacion=rd.GetString(4),
                                        PuestoJefeDirecto=rd.GetString(5),
                                        Email=rd.GetString(6)
                                    }); 
                                }

                                conn.Close();
                                return Json(vacantesrequisitos.ToList());
                            }
                            else {
                                return Json(new {msg= "Ha ocurrido un error al consultar los Requisitos, por favor intentalo mas tarde." });
                            }
                        }
                    }
                    else
                    {
                        return Json(new { msg = "Token no Valido" });
                    }
                }
                else
                {
                    return Json(new { msg = "Problema al verifcar el Token" });
                }
            }
            catch (Exception e)
            {

                return Json(new { msg = e.Message.ToString() });
            }

        }

        [HttpPost]
        [ActionName("postObtenerVacantes")]
        public IHttpActionResult postObtenerVacantes(Vacantes vacantes) {
          
            try
            {
                IEnumerable<string> headerValues;
                if (Request.Headers.TryGetValues("token", out headerValues)) {
                    if (ValidacionToken(headerValues.FirstOrDefault()) > 0)
                    {
                        using (SqlConnection conn =Conexion())
                        {

                            if (Convert.ToInt32(vacantes.EdadMinima)<18)
                            {
                                return Json(new {msg= "Por el momento no contamos con vacantes de acuerdo a tu perfíl, permítenos conservar tu información enviando tu CV al correo bolsadeempleo@jumex.com.mx Nos pondremos en contacto contigo en cuanto tengamos una vacante de acuerdo a tu perfil. Que tengas excelente día. 😊" });
                            }

                            List<Vacantes> vacantesdisponibles = new List<Vacantes>();
                            conn.Open();
                            string[] ubicacion = vacantes.Ubicacion.Split(' ');
                            SqlCommand cmd = new SqlCommand("SELECT Puesto,EdadMinima,Ubicacion,Experiencia,JefeDirecto FROM JumexBot_Vacantes WHERE Cercanias like @UBICACION AND Escolaridad like @EDUCACION AND Estatus=0 AND Area=@AREA",conn);
                            cmd.Parameters.AddWithValue("@UBICACION","%"+ubicacion[0]+"%");
                            cmd.Parameters.AddWithValue("@EDUCACION","%"+vacantes.Escolaridad+"%");
                            cmd.Parameters.AddWithValue("@AREA",vacantes.area);
                            SqlDataReader rd = cmd.ExecuteReader();
                            if (rd.HasRows)
                            {
                                while (rd.Read())
                                {
                                    vacantesdisponibles.Add(new Vacantes
                                    {
                                        Puesto=rd.GetString(0),
                                        EdadMinima=rd.GetString(1),
                                        Ubicacion=rd.GetString(2),
                                        Experiencia=rd.GetString(3),
                                        JefeDirecto=rd.GetString(4)
                                        
                                    });
                                }
                                conn.Close();
                                return Json(vacantesdisponibles.ToList());
                            }
                            else
                            {
                                conn.Close();
                                return Json(new { msg = "Por el momento no contamos con vacantes disponibles en esta localidad, permítenos conservar tu información, envíanos tu CV al correo bolsadeempleo@jumex.com.mx Nos pondremos en contacto contigo en cuanto tengamos una vacante de acuerdo a tu perfil. Que tengas excelente día 😊" });
                            }
                        }
                    }
                    else
                    {
                        return Json(new { msg = "Token no Valido" });
                       
                    }
                }
                else
                {
                    return Json(new { msg = "Token no Valido" });
                   
                }
            }
            catch (Exception e)
            {

                return Json(new { msg = e.Message.ToString()});
            }
        }

        [HttpPost]
        [ActionName("postContactoRegistro")]

        public IHttpActionResult postContactoRegistro(Registro registro) {

           

            try
            {
                IEnumerable<string> headerValues;
                if (Request.Headers.TryGetValues("token", out headerValues))
                {
                    if (ValidacionToken(headerValues.FirstOrDefault())>0)
                    {
                        using (SqlConnection conn= Conexion())
                        {
                            conn.Open();
                            SqlCommand cmd = new SqlCommand("INSERT INTO JumexBot_RegistrosContacto VALUES(@FECHACONTRATO,@MOTIVO,@ESTADO); SELECT SCOPE_IDENTITY();",conn);
                            cmd.Parameters.AddWithValue("@FECHACONTRATO",DateTime.Now);
                            cmd.Parameters.AddWithValue("@MOTIVO",registro.Motivo);
                            cmd.Parameters.AddWithValue("@ESTADO",registro.Estado);
                            int id=Convert.ToInt32(cmd.ExecuteScalar());
                            conn.Close();

                            return Json(new { msg = id });
                        }
                    }
                    else
                    {
                        return Json(new { msg = "Token no Valido" });
                    }
                }
                else
                {
                    return Json(new { msg = "Error al Recibir el Token" });
                }

            }
            catch (Exception e)
            {

                return Json(new { msg = e.Message.ToString() });
            }

        }

        // GET api/values
        [HttpPost]
        [ActionName("postCandidato")]
        public IHttpActionResult postCandidato(Candidato candidato)
        {
            
           
            try
            {
                IEnumerable<string> headerValues;
                if (Request.Headers.TryGetValues("token",out headerValues))
                {
                    if (ValidacionToken(headerValues.FirstOrDefault())>0)
                    {
                        using (SqlConnection conn = Conexion())
                        {
                            conn.Open();
                            SqlCommand cmd = new SqlCommand("JumexBot_PostInsertarCandidato", conn);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@NOMBRE", candidato.Nombre);
                            cmd.Parameters.AddWithValue("@EDAD", candidato.Edad);
                            cmd.Parameters.AddWithValue("@UBICACION", candidato.Ubicacion);
                            cmd.Parameters.AddWithValue("@ESTUDIOS", candidato.Estudios);
                            cmd.Parameters.AddWithValue("@ACEPTACONTRATO", candidato.AceptaContrato);
                            cmd.Parameters.AddWithValue("@PUESTO", candidato.Puesto);
                            cmd.Parameters.AddWithValue("@EMAIL", candidato.Email);
                            cmd.Parameters.AddWithValue("@JEFE",candidato.Jefe);
                            cmd.Parameters.AddWithValue("@REGISTROID",candidato.Registroid);
                            cmd.Parameters.AddWithValue("@AREA",candidato.Area);
                            cmd.Parameters.Add("@MENSAJE", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                            cmd.Parameters.Add("@ID_REGISTRO",SqlDbType.Int).Direction=ParameterDirection.Output;
                            cmd.ExecuteNonQuery();
                            conn.Close();
                            var Mensaje = cmd.Parameters["@MENSAJE"].Value.ToString();
                            var IdRegistro = cmd.Parameters["@ID_REGISTRO"].Value;
                            return Json(new { msg = Mensaje, IdRegistro });

                        }
                    }
                    else
                    {
                        return Json(new { msg = "Token no Valido" });
                    }
                }
                else
                {
                    return Json(new { msg = "Error al Recibir el Token" });
                }

            }
            catch (Exception e)
            {

                return Json(new { msg = e.Message.ToString() });
            }
        }

        [HttpPost]
        [ActionName("postIniciarSesion")]
        public IHttpActionResult postIniciarSesion(LoginBot login) {
            try
            {
                IEnumerable<string> headerValues;
                if (Request.Headers.TryGetValues("token", out headerValues)) {
                    if (ValidacionToken(headerValues.FirstOrDefault()) > 0)
                    {
                        using (SqlConnection conn = Conexion())
                        {
                            conn.Open();
                            SqlCommand cmd = new SqlCommand("SELECT Nombre FROM JumexBot_Usuarios WHERE No_Empleado=@USUARIO AND Contraseña=@CONTRASEÑA AND Activo=0",conn);
                            cmd.Parameters.AddWithValue("@USUARIO",login.Usuario);
                            cmd.Parameters.AddWithValue("@CONTRASEÑA",login.Contraseña);
                            SqlDataReader rd = cmd.ExecuteReader();
                            var nombre="";
                            if (rd.Read())
                            {
                                nombre = rd.GetString(0);
                                conn.Close();
                                return Json(new { msg = nombre});
                            }
                            else
                            {
                                conn.Close();
                                return Json(new { msg = "el Usuario no existe o es incorrecto" });                            
                            }

                        }

                    }
                    else
                    {
                        return Json(new { msg = "Token no Valido" });
                    }
                }
                else
                {
                    return Json(new { msg = "Error al Recibir el Token" });
                }
            }
            catch (Exception e)
            {

                return Json(new { msg = e.Message.ToString() });
            }

        }

        [HttpPost]
        [ActionName("postVacantes")]
        public IHttpActionResult postVacantes(Vacantes vacantes) {


            try
            {
                IEnumerable<string> headerValues;
                if (Request.Headers.TryGetValues("token", out headerValues))
                {
                    if (ValidacionToken(headerValues.FirstOrDefault()) > 0)
                    {
                        using (SqlConnection conn = Conexion())
                        {
                            conn.Open();
                            SqlCommand cmd = new SqlCommand("JumexBot_PostInsertarVacantes",conn);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@IDVACANTE",vacantes.IdVacante);
                            cmd.Parameters.AddWithValue("@PUESTO",vacantes.Puesto);
                            cmd.Parameters.AddWithValue("@ESCOLARIDAD",vacantes.Escolaridad);
                            cmd.Parameters.AddWithValue("@EDADMINIMA",vacantes.EdadMinima);
                            cmd.Parameters.AddWithValue("@UBICACION",vacantes.Ubicacion);
                            cmd.Parameters.AddWithValue("@EXPERIENCIA",vacantes.Experiencia);
                            cmd.Parameters.AddWithValue("@JEFEDIRECTO",vacantes.JefeDirecto);
                            cmd.Parameters.AddWithValue("@PUESTOJEFEDIRECTO", vacantes.PuestoJefeDirecto);
                            cmd.Parameters.AddWithValue("@EMAIL",vacantes.Email);
                            cmd.Parameters.AddWithValue("@AREA",vacantes.area);
                            cmd.Parameters.Add("@MENSAJE",SqlDbType.VarChar,250).Direction=ParameterDirection.Output;
                            cmd.ExecuteNonQuery();
                            conn.Close();
                            var Mensaje = cmd.Parameters["@MENSAJE"].Value.ToString();
                            return Json(new { msg = Mensaje });
                        }
                    }
                    else
                    {
                        return Json(new { msg = "Token no Valido" });
                    }
                }
                else
                {
                    return Json(new { msg = "Error al Recibir el Token" });
                }
            }
            catch (Exception e)
            {

                return Json(new { msg = e.Message.ToString() });
            }
        }

        [HttpPost]
        [ActionName("postEstatus")]
        public IHttpActionResult postEstatus(Vacantes vacantes) {
      
            try
            {
                IEnumerable<string> headerValues;
                if (Request.Headers.TryGetValues("token", out headerValues))
                {
                    if (ValidacionToken(headerValues.FirstOrDefault()) > 0)
                    {
                        using (SqlConnection conn = Conexion())
                        {
                            conn.Open();
                            SqlCommand cmd = new SqlCommand("JumexBot_UpdateEditarVacantes", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@IDVACANTE", vacantes.IdVacante);
                            cmd.Parameters.AddWithValue("@ESTATUS",vacantes.Estatus);
                            cmd.Parameters.Add("@MENSAJE",SqlDbType.VarChar,50).Direction=ParameterDirection.Output;
                            cmd.ExecuteNonQuery();
                            var Mensaje = cmd.Parameters["@MENSAJE"].Value.ToString();
                            conn.Close();

                            return Json(new { msg = Mensaje });
                        }
                    }
                    else
                    {
                        return Json(new { msg = "Token no Valido" });
                    }
                }
                else
                {
                    return Json(new { msg = "Error al Recibir el Token" });
                }
            }
            catch (Exception e)
            {

                return Json(new { msg = e.Message.ToString() });
            }
        }

        [HttpPost]
        [ActionName("postValidacion")]
        public IHttpActionResult postValidacion(LoginBot bot) {
       
            try
            {
                byte[] decodedBytes = Convert.FromBase64String(bot.Contraseña);
                string contraseña = System.Text.ASCIIEncoding.ASCII.GetString(decodedBytes);
                
                if (bot.Usuario.Equals(null) || contraseña.Equals(null))
                {
                   

                    return Json(new { msg = "Ningun Campo puede ir Vacio" });
                }
                using (SqlConnection con = Conexion())
                {
                    con.Open();

                    SqlCommand cmd = new SqlCommand("SELECT * FROM JumexBot_CuentaApi WHERE NombreBot=@USUARIO AND Contraseña=@CONTRASEÑA", con);
                    cmd.Parameters.AddWithValue("@USUARIO",bot.Usuario);
                    cmd.Parameters.AddWithValue("@CONTRASEÑA",contraseña);
                    cmd.ExecuteNonQuery();
                    SqlDataReader rd = cmd.ExecuteReader();
                    if (rd.HasRows)
                    {
                        var theString = Guid.NewGuid().ToString();
                        byte[] theBytes = Encoding.UTF8.GetBytes(theString);
                        DateTime time = DateTime.Now.AddHours(24);
                        SqlCommand token = new SqlCommand("UPDATE JumexBot_TokenApi SET Token=@TOKEN, TokenExpiracion=@TOKENEXPIRACION WHERE Usuario=@USUARIO", con);
                        token.Parameters.AddWithValue("@USUARIO",bot.Usuario);
                        token.Parameters.AddWithValue("@TOKEN",theBytes);
                        token.Parameters.AddWithValue("@TOKENEXPIRACION",time);
                        token.ExecuteNonQuery();
                        con.Close();
                        return Json(new { token = theString});
                    }
                    else
                    {
                        return Json(new { msg = "No Existen Usuarios Reigstrados en el Sistema" });
                    }


                }

            }
            catch (Exception e)
            {

                return Json(new { msg = e.Message.ToString() });
            }



        }

        [HttpPost]
        [ActionName("postCorreo")]

        public IHttpActionResult postCorreo(Candidato candidato) {
            try
            {
                IEnumerable<string> headerValues;
                if (Request.Headers.TryGetValues("token", out headerValues))
                {
                    if (ValidacionToken(headerValues.FirstOrDefault()) > 0)
                    {
                        using (SqlConnection conn=Conexion())
                        {
                            conn.Open();
                            SqlCommand cmd = new SqlCommand("UPDATE JumexBot_Candidato SET Email=@EMAIL WHERE Id=@ID",conn);
                            cmd.Parameters.AddWithValue("@EMAIL",candidato.Email);
                            cmd.Parameters.AddWithValue("@ID",candidato.Id);
                            cmd.ExecuteNonQuery();
                            conn.Close();
                            return Json(new {msg="OK"});
                        }
                    }
                    else
                    {
                        return Json(new {msg="Token no válido."});
                    }
                }
                else
                {
                    return Json(new {msg="Error en el token"});
                }
            }
            catch (Exception e)
            {

                return Json(new {msg=e.Message.ToString()});
            }
        }

        [HttpPost]
        [ActionName("postConsultaEducacion")]

        public IHttpActionResult postConsultaEducacion(Vacantes vacantes) {

            try
            {
                IEnumerable<string> headerValues;
                if (Request.Headers.TryGetValues("token", out headerValues))
                {
                    if (ValidacionToken(headerValues.FirstOrDefault()) > 0)
                    {

                        using (SqlConnection conn=Conexion())
                        {
                            conn.Open();
                            SqlCommand cmd = new SqlCommand("Select * From JumexBot_Vacantes where Escolaridad like @EDUCACION AND Area=@AREA AND Estatus=0",conn);
                            cmd.Parameters.AddWithValue("@EDUCACION",vacantes.Escolaridad);
                            cmd.Parameters.AddWithValue("@AREA",vacantes.area);
                            SqlDataReader rd = cmd.ExecuteReader();
                           
                            if (rd.HasRows)
                            {
                                conn.Close();
                                return Json(new {resultado=1});
                            }
                            else
                            {
                                conn.Close();
                                return Json(new {resultado=0 });
                            }

                        }

                    }
                    else
                    {
                        return Json(new {msg="El token no es valido" });
                    }
                }
                else
                {
                    return Json(new {msg="Error en el token" });
                }
            }
            catch (Exception e)
            {

                return Json(new {msg=e.Message.ToString()});
            }

        }
        public int ValidacionToken(string token) {

            using (SqlConnection con=Conexion())
            {
                con.Open();
                byte[] theBytes = Encoding.UTF8.GetBytes(token);
                SqlCommand cmd = new SqlCommand("SELECT * FROM JumexBot_TokenApi WHERE Token=@TOKEN AND TokenExpiracion >= @TOKENEXPIRACION", con);
                cmd.Parameters.AddWithValue("@TOKEN",theBytes);
                cmd.Parameters.AddWithValue("@TOKENEXPIRACION",DateTime.Now);
                SqlDataReader rd = cmd.ExecuteReader();
                int i = 0;
                if (rd.HasRows)
                {
                    return i = 1;
                }
                return i;
            }

        }


        
    }
}
