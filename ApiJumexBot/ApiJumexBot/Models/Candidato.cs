﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiJumexBot.Models
{
    public class Candidato
    {
     
        public int Id { get; set; }
        public string Nombre { get; set; }

        public int Edad { get; set; }

        public string Ubicacion { get; set; }

        public string Estudios { get; set; }

        public string AceptaContrato { get; set; }

        public string Puesto { get; set; }

        public string Email { get; set; }

        public string FechaEntrevista { get; set; }

        public string Jefe { get; set; }

        public int Registroid { get; set; }

        public string Area { get; set; }

    }
}