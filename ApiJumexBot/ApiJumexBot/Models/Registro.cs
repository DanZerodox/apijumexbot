﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiJumexBot.Models
{
    public class Registro
    {

        public DateTime FechaContacto { get; set; }
        public string Motivo { get; set; }

        public int Estado { get; set; }

    }
}