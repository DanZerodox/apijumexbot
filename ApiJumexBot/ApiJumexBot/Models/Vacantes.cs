﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiJumexBot.Models
{
    public class Vacantes
    {

        public string IdVacante { get; set; }

        public string Puesto { get; set; }

        public string Escolaridad { get; set; }

        public string Requisitos { get; set; }

        public string EdadMinima { get; set; }
        public string Ubicacion { get; set; }

        public string Experiencia { get; set; }

        public string JefeDirecto { get; set; }

        public int Estatus { get; set; }

        public string PuestoJefeDirecto { get; set; }

        public string Email { get; set; }

        public string area { get; set; }

    }
}