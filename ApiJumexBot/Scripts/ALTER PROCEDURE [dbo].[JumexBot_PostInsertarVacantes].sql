USE [Espacio_Jumex]
GO
/****** Object:  StoredProcedure [dbo].[JumexBot_PostInsertarVacantes]    Script Date: 23/08/2019 16:24:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[JumexBot_PostInsertarVacantes]
@IDVACANTE VARCHAR(50),
@PUESTO VARCHAR(50),
@ESCOLARIDAD VARCHAR(50),
@EDADMINIMA VARCHAR(50),
@UBICACION VARCHAR(250),
@EXPERIENCIA VARCHAR(50),
@JEFEDIRECTO VARCHAR(50),
@MENSAJE VARCHAR(250) OUT
AS
	DECLARE @EXISTE INT 
    SELECT @EXISTE= Estatus FROM JumexBot_Vacantes WHERE IdVacante=@IDVACANTE

	IF @EXISTE IS NOT NULL AND @EXISTE <> 1
	BEGIN 
		SET @MENSAJE='Ya existe una Vacante en el sistema con este Identificador y se encuentra INACTIVA'
    END
	
	IF @EXISTE IS NOT NULL AND @EXISTE = 0
	BEGIN
		SET @MENSAJE='Ya existe una Vacante en el sistema con este Identificador y se encuentra ACTIVA'
	END
	ELSE 
	BEGIN 
		INSERT INTO JumexBot_Vacantes VALUES(@IDVACANTE,@PUESTO,@ESCOLARIDAD,@EDADMINIMA,@UBICACION,@EXPERIENCIA,@JEFEDIRECTO,0)
		SET @MENSAJE='Se ha registrado de manera correcta la vacante '+@PUESTO

	END