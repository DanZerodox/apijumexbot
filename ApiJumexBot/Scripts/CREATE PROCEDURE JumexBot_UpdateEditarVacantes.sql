USE [JumexBot]
GO

/****** Object:  StoredProcedure [dbo].[JumexBot_UpdateEditarVacantes]    Script Date: 22/08/2019 15:47:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[JumexBot_UpdateEditarVacantes]
@IDVACANTE VARCHAR(50),
@ESTATUS INT,
@MENSAJE VARCHAR(50) OUT
AS
	DECLARE @EXISTE INT
	SELECT @EXISTE= Id FROM JumexBot_Vacantes WHERE IdVacante=@IDVACANTE

	IF @EXISTE IS NULL
	BEGIN
		SET @MENSAJE='La Vacante con el Id '+@IDVACANTE+' no existe.'
	END
	ELSE BEGIN
		UPDATE JumexBot_Vacantes SET Estatus=@ESTATUS WHERE IdVacante=@IDVACANTE
		SET @MENSAJE='El estatus de la Vacante de ha modificado correctamente.'
	END
GO


