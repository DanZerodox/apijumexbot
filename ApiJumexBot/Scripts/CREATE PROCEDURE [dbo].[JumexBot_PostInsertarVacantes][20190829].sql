------------------------------------------------------------------------------------------------------------
-- BASE DE DATOS
------------------------------------------------------------------------------------------------------------
USE [Espacio_Jumex]
GO
------------------------------------------------------------------------------------------------------------
-- CREACION DE RESPALDO
------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID('[dbo].[JumexBot_PostInsertarVacantes]_Bkup20190826') AND SYSSTAT & 0XF = 4)
    BEGIN
	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID('[dbo].[JumexBot_PostInsertarVacantes]') AND SYSSTAT & 0XF = 4)
	    BEGIN
		EXEC SP_RENAME '[dbo].[JumexBot_PostInsertarVacantes]', '[dbo].[JumexBot_PostInsertarVacantes]_Bkup20190829'
		PRINT '<<< RENAME SP NameSP >>>'
	    END
    END
GO
------------------------------------------------------------------------------------------------------------
-- CREACION DE SP
------------------------------------------------------------------------------------------------------------
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[JumexBot_PostInsertarVacantes]
@IDVACANTE VARCHAR(50),
@PUESTO VARCHAR(50),
@ESCOLARIDAD VARCHAR(50),
@EDADMINIMA VARCHAR(50),
@UBICACION VARCHAR(250),
@EXPERIENCIA VARCHAR(50),
@JEFEDIRECTO VARCHAR(50),
@PUESTOJEFEDIRECTO VARCHAR(50),
@EMAIL VARCHAR(50),
@AREA VARCHAR(50),
@MENSAJE VARCHAR(250) OUT
AS
BEGIN
-- CONSECUTIVO 000001
-- -----------------------------------------------------------------------------------------
-- CONSECUTIVO	:	000001
-- BASE DE DATOS	:	Espacio_Jumex
-- MODULO		:	JOB
-- OBJETIVO		:	Modificar la Inserción de la Vacantes
--
-- COMENTARIOS	: 
--				 
-- 
-- CONSECUTIVO		AUTOR						FECHA		MODIFICACION
-- -----------------------------------------------------------------------------------------------
-- 000001			Daniel Mendoza Mancilla	    26/08/2019	Creacion.
-- 000002			Daniel Mendoza Mancilla		29/08/2019	Modificación.
-- -----------------------------------------------------------------------------------------------
-- Pruebas Unitarias
--	EXEC [dbo].[JumexBot_PostInsertarVacantes]
-- -----------------------------------------------------------------------------------------------
-- DECLARACION DE VARIABLES
-- -----------------------------------------------------------------------------------------------
	DECLARE @EXISTE INT 
------------------------------------------
-- DECLARACION DE REGISTROS EXISTENTES
------------------------------------------
    SELECT @EXISTE= Estatus FROM JumexBot_Vacantes WHERE IdVacante=@IDVACANTE

	IF @EXISTE IS NOT NULL AND @EXISTE <> 1
	BEGIN 
		SET @MENSAJE='Ya existe una Vacante en el sistema con este Identificador y se encuentra INACTIVA'
    END
	
	IF @EXISTE IS NOT NULL AND @EXISTE = 0
	BEGIN
		SET @MENSAJE='Ya existe una Vacante en el sistema con este Identificador y se encuentra ACTIVA'
	END
	ELSE 
	BEGIN 
------------------------------------------
-- Insert
------------------------------------------
		INSERT INTO JumexBot_Vacantes(IdVacante,Puesto,Escolaridad,EdadMinima,Ubicacion,Experiencia,JefeDirecto,Estatus,PuestoJefeDirecto,Email,Area)VALUES(@IDVACANTE,@PUESTO,@ESCOLARIDAD,@EDADMINIMA,@UBICACION,@EXPERIENCIA,@JEFEDIRECTO,0,@PUESTOJEFEDIRECTO,@EMAIL,@AREA)
		SET @MENSAJE='Se ha registrado de manera correcta la vacante '+@PUESTO

	END
END
-----------------------------------------------------------------------------------------------
-- FIN
------------------------------------------------------------------------------------------------
GRANT EXECUTE ON JumexBot_PostInsertarVacantes TO Espacio_Jumex
GO 