USE [JumexBot]
GO

/****** Object:  Table [dbo].[JumexBot_Vacantes]    Script Date: 22/08/2019 15:46:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JumexBot_Vacantes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdVacante] [varchar](50) NOT NULL,
	[Puesto] [varchar](50) NOT NULL,
	[Escolaridad] [varchar](50) NOT NULL,
	[EdadMinima] [int] NOT NULL,
	[EdadMaxima] [int] NOT NULL,
	[Ubicacion] [varchar](250) NOT NULL,
	[Experiencia] [varchar](50) NOT NULL,
	[JefeDirecto] [varchar](50) NOT NULL,
	[Estatus] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


