------------------------------------------------------------------------------------------------------------
-- BASE DE DATOS
------------------------------------------------------------------------------------------------------------
USE Espacio_Jumex
GO

------------------------------------------------------------------------------------------------------------
-- CREACION DE RESPALDO
------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID('dbo.JumexBot_PostInsertarCandidato_Bkup20190909') AND SYSSTAT & 0XF = 4)
    BEGIN
	IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID('dbo.JumexBot_PostInsertarCandidato') AND SYSSTAT & 0XF = 4)
	    BEGIN
		EXEC SP_RENAME 'JumexBot_PostInsertarCandidato', 'JumexBot_PostInsertarCandidato_Bkup20190909'
		PRINT '<<< RENAME SP JumexBot_PostInsertarCandidato >>>'
	    END
    END
GO

------------------------------------------------------------------------------------------------------------
-- CREACION DE SP
------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM SYSOBJECTS WHERE ID = OBJECT_ID('dbo.JumexBot_PostInsertarCandidato') AND SYSSTAT & 0XF = 4)
    BEGIN
	DROP PROCEDURE dbo.NameSP
	PRINT '<<< DROPPED SP dbo.JumexBot_PostInsertarCandidato >>>'
    END
GO
CREATE PROCEDURE [dbo].[JumexBot_PostInsertarCandidato]
@NOMBRE VARCHAR(30),
@EDAD INT,
@UBICACION VARCHAR(250),
@ESTUDIOS VARCHAR(50),
@ACEPTACONTRATO VARCHAR(2),
@PUESTO VARCHAR(50),
@EMAIL VARCHAR(120),
@JEFE VARCHAR(50),
@REGISTROID INT,
@AREA VARCHAR(50),
@MENSAJE VARCHAR(120) OUT,
@ID_REGISTRO INT OUT
AS
BEGIN
-- CONSECUTIVO 000001
-- -----------------------------------------------------------------------------------------
-- CONSECUTIVO	:	000001
-- BASE DE DATOS	:	Espacio_Jumex
-- MODULO		:	JOB
-- OBJETIVO		:	Crear la Inserción de los candidatos y registrarlos en la agenda de su ubicacion
--					correspondiente.
--
-- COMENTARIOS	: 
--				 
-- 
-- CONSECUTIVO		AUTOR						FECHA		MODIFICACION
-- -----------------------------------------------------------------------------------------------
-- 000001			Daniel Mendoza Mancilla	    05/09/2019	Creacion.
-- 000002			Daniel Mendoza Mancilla		06/09/2019	Se modifica el agendado de candidatos por horarios.
-- -----------------------------------------------------------------------------------------------
-- Pruebas Unitarias
--	EXEC [dbo].[JumexBot_PostInsertarCandidato]
-- -----------------------------------------------------------------------------------------------
-- DECLARACION DE VARIABLES
-- -----------------------------------------------------------------------------------------------
	    DECLARE @EXISTEREGISTRO INT,
				@EXISTEREGISTRO2 INT,
				@FECHAREGISTRAR DATE,
				@HORARIO varchar(120),
				@HORARIOALTERNO varchar(120),
				@LIMITECANDIDATOS INT,
				@FECHAINCREMENTO DATE,
				@FECHAINCREMENTOVIERNES DATE,
				@DIA VARCHAR(120),
				@FECHACTUAL DATE
------------------------------------------
-- CONSULTAR HORARIOS REIGSTRADOS POR MEDIO DE LA UBICACION
------------------------------------------
		SELECT TOP(1) @HORARIO=Horario FROM JumexBot_Horarios where Ubicacion LIKE '%'+@UBICACION+'%'
		SELECT TOP(1) @FECHAREGISTRAR=FechaEntrevista FROM JumexBot_Registros_Horarios WHERE Ubicacion LIKE '%'+@UBICACION+'%' AND Horario=@HORARIO order by FechaEntrevista desc

------------------------------------------
-- GENERAR UNA NUEVA FECHA DE ENTREVISTA SI NO EXISTE NINGUNA PREVIA EN LA TABLA
------------------------------------------
		IF @FECHAREGISTRAR IS NULL
		BEGIN
------------------------------------------
-- SE VALIDA QUE EL DIA NO SEA SABADO O DOMINGO
------------------------------------------
				IF @FECHAREGISTRAR<=CONVERT(DATE,GETDATE())
				BEGIN
					SELECT @FECHAREGISTRAR=DATEADD(DAY,1,GETDATE())
				END
				ELSE IF(DATEPART(WEEKDAY,@FECHAREGISTRAR)=6)
				BEGIN
					SELECT @FECHAREGISTRAR=DATEADD(DAY,2,GETDATE())
				END
				ELSE 
				BEGIN
					SELECT @FECHAREGISTRAR=DATEADD(DAY,1,GETDATE())
				END
				
		END

		SELECT @DIA=FORMAT(@FECHAREGISTRAR,'dddd','es-es')
------------------------------------------
-- OBTENER LOS CANDIDATOS QUE SE PUEDEN RECIBIR POR DIA Y VALIDAR SI EXISTE ALGUN CANDIDATO REGISTRADO
------------------------------------------
		SELECT @LIMITECANDIDATOS=CantidadporDia FROM JumexBot_Horarios where Ubicacion LIKE '%'+@UBICACION+'%' AND Horario=@HORARIO
		SELECT @EXISTEREGISTRO=Candidatos FROM JumexBot_Registros_Horarios WHERE Ubicacion LIKE '%'+@UBICACION+'%' AND FechaEntrevista=@FECHAREGISTRAR AND Horario=@HORARIO

------------------------------------------
-- SE VALIDA SI EXISTE ALGUN REGISTRO Y DE LO CONTRARIO SE INSERTA UNO
------------------------------------------
		IF @EXISTEREGISTRO IS NULL
		BEGIN
------------------------------------------
-- Insert
------------------------------------------
			IF @FECHAREGISTRAR<=CONVERT(DATE,GETDATE())
				BEGIN
					SELECT @FECHAREGISTRAR=DATEADD(DAY,1,GETDATE())
				END
			INSERT INTO JumexBot_Registros_Horarios(Ubicacion,Horario,FechaEntrevista,Candidatos)
			Values(@UBICACION,@HORARIO,@FECHAREGISTRAR,1)
			INSERT INTO JumexBot_Candidato(Nombre,Edad,Ubicacion,Estudios,AceptaContrato,Puesto,Email,FechaRegistro,FechaEntrevista,Jefe,Registroid,Area) VALUES(@NOMBRE,@EDAD,@UBICACION,@ESTUDIOS,@ACEPTACONTRATO,@PUESTO,@EMAIL,GETDATE(),@FECHAREGISTRAR,@JEFE,@REGISTROID,@AREA)
			SET @ID_REGISTRO=SCOPE_IDENTITY()
			SET @MENSAJE='el día '+FORMAT(@FECHAREGISTRAR,'dddd','es-es')+' '+CAST(DAY(@FECHAREGISTRAR)AS VARCHAR)+' de '+FORMAT(@FECHAREGISTRAR,'MMMM')+' del 2019 en un horario de '+@HORARIO
			
		END
------------------------------------------
-- SE VALIDA SI AUN ESTA DENTRO DEL LIMITE DE LOS CANDIDATOS RECIBIDOS POR DIA
------------------------------------------
	    IF @EXISTEREGISTRO < @LIMITECANDIDATOS
		BEGIN
------------------------------------------
-- Update
------------------------------------------
			IF @FECHAREGISTRAR<=CONVERT(DATE,GETDATE())
				BEGIN
					SELECT @FECHAREGISTRAR=DATEADD(DAY,1,GETDATE())
					INSERT INTO JumexBot_Registros_Horarios(Ubicacion,Horario,FechaEntrevista,Candidatos)
					VALUES(@UBICACION,@HORARIO,@FECHAREGISTRAR,1)
				END
				ELSE 
				BEGIN
					UPDATE JumexBot_Registros_Horarios SET Candidatos=@EXISTEREGISTRO+1 WHERE Ubicacion=@UBICACION AND FechaEntrevista=@FECHAREGISTRAR AND Horario=@HORARIO
				END	
------------------------------------------
-- Insert
------------------------------------------
			INSERT INTO JumexBot_Candidato(Nombre,Edad,Ubicacion,Estudios,AceptaContrato,Puesto,Email,FechaRegistro,FechaEntrevista,Jefe,Registroid,Area) VALUES(@NOMBRE,@EDAD,@UBICACION,@ESTUDIOS,@ACEPTACONTRATO,@PUESTO,@EMAIL,GETDATE(),@FECHAREGISTRAR,@JEFE,@REGISTROID,@AREA)
			SET @ID_REGISTRO=SCOPE_IDENTITY()
			SET @MENSAJE='el día '+FORMAT(@FECHAREGISTRAR,'dddd','es-es')+' '+CAST(DAY(@FECHAREGISTRAR)AS VARCHAR)+' de '+FORMAT(@FECHAREGISTRAR,'MMMM')+' del 2019 en un horario de '+@HORARIO
		END
------------------------------------------
-- SI LOS CANDIDATOS SUPEREN EL LIMITE POR DIA, SE REVISA SI EXISTE ALGUN OTRO HORARIO DISPONIBLE
------------------------------------------
	    IF @EXISTEREGISTRO >= @LIMITECANDIDATOS
		BEGIN
			SELECT TOP(1) @HORARIOALTERNO=Horario FROM JumexBot_Horarios where Ubicacion LIKE '%'+@UBICACION+'%' order by Id desc
			SELECT @LIMITECANDIDATOS=CantidadporDia FROM JumexBot_Horarios where Ubicacion LIKE '%'+@UBICACION+'%' AND Horario=@HORARIOALTERNO
			SELECT @EXISTEREGISTRO2=Candidatos FROM JumexBot_Registros_Horarios WHERE Ubicacion LIKE '%'+@UBICACION+'%' AND FechaEntrevista=@FECHAREGISTRAR AND Horario=@HORARIOALTERNO
			IF @EXISTEREGISTRO2 IS NULL
			BEGIN
------------------------------------------
-- Insert
------------------------------------------
				INSERT INTO JumexBot_Registros_Horarios(Ubicacion,Horario,FechaEntrevista,Candidatos)
				Values(@UBICACION,@HORARIOALTERNO,@FECHAREGISTRAR,1)
				INSERT INTO JumexBot_Candidato(Nombre,Edad,Ubicacion,Estudios,AceptaContrato,Puesto,Email,FechaRegistro,FechaEntrevista,Jefe,Registroid,Area) VALUES(@NOMBRE,@EDAD,@UBICACION,@ESTUDIOS,@ACEPTACONTRATO,@PUESTO,@EMAIL,GETDATE(),@FECHAREGISTRAR,@JEFE,@REGISTROID,@AREA)
				SET @ID_REGISTRO=SCOPE_IDENTITY()
				SET @MENSAJE='el día '+FORMAT(@FECHAREGISTRAR,'dddd','es-es')+' '+CAST(DAY(@FECHAREGISTRAR)AS VARCHAR)+' de '+FORMAT(@FECHAREGISTRAR,'MMMM')+' del 2019 en un horario de '+@HORARIOALTERNO
			END
------------------------------------------
-- SE VALIDA SI EL NUEVO HORARIO AUN TIENE DISPONIBILIDAD
------------------------------------------
			IF @EXISTEREGISTRO2 < @LIMITECANDIDATOS
			BEGIN
------------------------------------------
-- Update
------------------------------------------
				UPDATE JumexBot_Registros_Horarios SET Candidatos=@EXISTEREGISTRO2+1 WHERE Ubicacion LIKE '%'+@UBICACION+'%' AND FechaEntrevista=@FECHAREGISTRAR AND Horario=@HORARIOALTERNO
------------------------------------------
-- Insert
------------------------------------------
				INSERT INTO JumexBot_Candidato(Nombre,Edad,Ubicacion,Estudios,AceptaContrato,Puesto,Email,FechaRegistro,FechaEntrevista,Jefe,Registroid,Area) VALUES(@NOMBRE,@EDAD,@UBICACION,@ESTUDIOS,@ACEPTACONTRATO,@PUESTO,@EMAIL,GETDATE(),@FECHAREGISTRAR,@JEFE,@REGISTROID,@AREA)
				SET @ID_REGISTRO=SCOPE_IDENTITY()
				SET @MENSAJE='el día '+FORMAT(@FECHAREGISTRAR,'dddd','es-es')+' '+CAST(DAY(@FECHAREGISTRAR)AS VARCHAR)+' de '+FORMAT(@FECHAREGISTRAR,'MMMM')+' del 2019 en un horario de '+@HORARIOALTERNO

			END
------------------------------------------
-- SI YA NO EXISTE DISPONIBILIDAD EN EL NUEVO HORARIO SE INSERTA UN NUEVO REGISTRO CON OTRO DIA
------------------------------------------	
			IF @EXISTEREGISTRO2 >= @LIMITECANDIDATOS 
			BEGIN
				SELECT @FECHAINCREMENTO=DATEADD(DAY,1,@FECHAREGISTRAR)
				
------------------------------------------
-- SE VALIDA QUE NO SE REGISTRE UNA FECHA CON FINES DE SEMANA
------------------------------------------			
					IF(DATEPART(WEEKDAY,@FECHAINCREMENTO)=6)
					BEGIN
					SELECT @FECHAINCREMENTOVIERNES=DATEADD(DAY,2,@FECHAINCREMENTO)
------------------------------------------
-- Insert
------------------------------------------
					INSERT INTO JumexBot_Registros_Horarios(Ubicacion,Horario,FechaEntrevista,Candidatos)
					Values(@UBICACION,@HORARIO,@FECHAINCREMENTOVIERNES,1)
					INSERT INTO JumexBot_Candidato(Nombre,Edad,Ubicacion,Estudios,AceptaContrato,Puesto,Email,FechaRegistro,FechaEntrevista,Jefe,Registroid,Area) VALUES(@NOMBRE,@EDAD,@UBICACION,@ESTUDIOS,@ACEPTACONTRATO,@PUESTO,@EMAIL,GETDATE(),@FECHAINCREMENTOVIERNES,@JEFE,@REGISTROID,@AREA)
					SET @ID_REGISTRO=SCOPE_IDENTITY()
					SET @MENSAJE='el día '+FORMAT(@FECHAINCREMENTOVIERNES,'dddd','es-es')+' '+CAST(DAY(@FECHAINCREMENTOVIERNES)AS VARCHAR)+' de '+FORMAT(@FECHAINCREMENTOVIERNES,'MMMM')+' del 2019 en un horario de '+@HORARIO
					END
					ELSE 
					BEGIN
					SELECT @FECHAINCREMENTO=DATEADD(DAY,1,@FECHAREGISTRAR)
------------------------------------------
-- Insert
------------------------------------------
					INSERT INTO JumexBot_Registros_Horarios(Ubicacion,Horario,FechaEntrevista,Candidatos)
					Values(@UBICACION,@HORARIO,@FECHAINCREMENTO,1)
					INSERT INTO JumexBot_Candidato(Nombre,Edad,Ubicacion,Estudios,AceptaContrato,Puesto,Email,FechaRegistro,FechaEntrevista,Jefe,Registroid,Area) VALUES(@NOMBRE,@EDAD,@UBICACION,@ESTUDIOS,@ACEPTACONTRATO,@PUESTO,@EMAIL,GETDATE(),@FECHAINCREMENTO,@JEFE,@REGISTROID,@AREA)
					SET @ID_REGISTRO=SCOPE_IDENTITY()
					SET @MENSAJE='el día '+FORMAT(@FECHAINCREMENTO,'dddd','es-es')+' '+CAST(DAY(@FECHAINCREMENTO)AS VARCHAR)+' de '+FORMAT(@FECHAINCREMENTO,'MMMM')+' del 2019 en un horario de '+@HORARIO
					END
			
			END
			
			
		END
		

END
IF OBJECT_ID('dbo.JumexBot_PostInsertarCandidato') IS NOT NULL
    PRINT '<<< CREATED SP dbo.JumexBot_PostInsertarCandidato >>>'
ELSE
    PRINT '<<< FAILED CREATING SP dbo.JumexBot_PostInsertarCandidato >>>'
