
USE [ESPACIO_JUMEX]
GO
--SE AGREGA UNA NUEVA COLUMNA A LA TABLA JUMEXBOT_VACANTES
ALTER TABLE JumexBot_Vacantes ADD Cercanias varchar(250) null

--SE CREA UNA NUEVA TABLA LLAMADA JUMEXBOT_HORARIOS
/****** Object:  Table [dbo].[JumexBot_Horarios]    Script Date: 05/09/2019 15:46:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JumexBot_Horarios](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Ubicacion] [varchar](250) NOT NULL,
	[CantidadporDia] [int] NOT NULL,
	[Horario] [varchar](120) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

--SE CREA UNA NUEVA TABLA LLAMADA JUMEXBOT_REGISTROS_HORARIOS
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[JumexBot_Registros_Horarios](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Ubicacion] [varchar](250) NOT NULL,
	[Horario] [varchar](120) NULL,
	[FechaEntrevista] [date] NOT NULL,
	[Candidatos] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


